﻿using Dtos;
using Xunit;

namespace DtosUnitTests
{
    public class BoxEncoderTests
    {
        [Fact]
        public void EncoderRoundTripWorks()
        {
            Box box = TestBoxes.NewTestBox();
            string password = "Chickens";

            IBoxEncoder boxEncoder = new BoxEncoder();
            string encoded = boxEncoder.Encode(box, password);
            Box test = boxEncoder.Decode(encoded, password);

            Assert.True(TestBoxes.AreEqual(box, test));
        }
    }
}
