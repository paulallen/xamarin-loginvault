﻿using Dtos;
using System;
using System.Diagnostics;
using Xunit;

namespace DtosUnitTests
{
    public class EncrypterTests
    {
        private const byte PAYLOAD_SALT_A_LEN = 13;
        private const byte PAYLOAD_SALT_B_LEN = 7;
        private const byte BLOCKSIZE_BYTES = 16;
        private const byte KEYSIZE_BYTES = 32;
        private const byte MESSAGESIZE_BYTES = 128;

        [Fact]
        public void ReadBufferRefStartCorrect()
        {
            Encrypter encrypter = new Encrypter();
            byte[] ib1 = TestBuffers.NewTestBuffer(MESSAGESIZE_BYTES, 0xbc);

            int[] auLengths = { 1, 2, 4, 8, 16, 32 };
            foreach (int uLength in auLengths)
            {
                byte[] ibTest;
                int uStart = 0;
                for (int n = 0; n < MESSAGESIZE_BYTES / uLength; ++n)
                {
                    encrypter.ReadBuffer(ib1, out ibTest, ref uStart, uLength);

                    Assert.True(ibTest.Length == uLength);

                    int uTest = (n * uLength) + uLength;
                    Assert.True(uStart == uTest);
                }
            }
        }

        [Fact]
        public void ReadBufferOutputBytesCorrect()
        {
            byte[] ib1 = TestBuffers.NewTestBuffer(1, 0x10);
            byte[] ib2 = TestBuffers.NewTestBuffer(2, 0x20);
            byte[] ib3 = TestBuffers.NewTestBuffer(3, 0x30);
            byte[] ib4 = TestBuffers.NewTestBuffer(4, 0x40);

            //byte[] ibAll;
            //using (DataWriter dw = new DataWriter())
            //{
            //    dw.WriteBuffer(ib1);
            //    dw.WriteBuffer(ib2);
            //    dw.WriteBuffer(ib3);
            //    dw.WriteBuffer(ib4);

            //    ibAll = dw.DetachBuffer();
            //}

            var ibAll = new byte[ib1.Length + ib2.Length + ib3.Length + ib4.Length];
            var n = 0;
            for (int i = 0; i < ib1.Length; ++i)
            {
                ibAll[n] = ib1[i];
                ++n;
            }
            for (int i = 0; i < ib2.Length; ++i)
            {
                ibAll[n] = ib2[i];
                ++n;
            }
            for (int i = 0; i < ib3.Length; ++i)
            {
                ibAll[n] = ib3[i];
                ++n;
            }
            for (int i = 0; i < ib4.Length; ++i)
            {
                ibAll[n] = ib4[i];
                ++n;
            }

            int uStart = 0;
            byte[] ibTest1, ibTest2, ibTest3, ibTest4;
            Encrypter encrypter = new Encrypter();

            encrypter.ReadBuffer(ibAll, out ibTest1, ref uStart, 1);
            encrypter.ReadBuffer(ibAll, out ibTest2, ref uStart, 2);
            encrypter.ReadBuffer(ibAll, out ibTest3, ref uStart, 3);
            encrypter.ReadBuffer(ibAll, out ibTest4, ref uStart, 4);

            Assert.True(TestBuffers.AreEqual(ibTest1, ib1));
            Assert.True(TestBuffers.AreEqual(ibTest2, ib2));
            Assert.True(TestBuffers.AreEqual(ibTest3, ib3));
            Assert.True(TestBuffers.AreEqual(ibTest4, ib4));
        }

        [Fact]
        public void SaltedPasswordInExpectedOrder()
        {
            byte[] ibFirst = TestBuffers.NewTestBuffer(KEYSIZE_BYTES, 0xbc);
            byte[] ibSecond = TestBuffers.NewTestBuffer(KEYSIZE_BYTES, 0xde);

            Encrypter encrypter = new Encrypter();
            byte[] ibSalted = encrypter.GenerateSaltedPassword(ibFirst, ibSecond);


            int uStart = 0;
            byte[] ibTest1, ibTest2;
            encrypter.ReadBuffer(ibSalted, out ibTest1, ref uStart, KEYSIZE_BYTES);
            encrypter.ReadBuffer(ibSalted, out ibTest2, ref uStart, KEYSIZE_BYTES);

            Assert.True(ibSalted.Length == ibFirst.Length + ibSecond.Length);
            Assert.True(TestBuffers.AreEqual(ibTest1, ibFirst));
            Assert.True(TestBuffers.AreEqual(ibTest2, ibSecond));
        }

        [Fact]
        public void PayloadAssemblyRoundTripWorks()
        {
            byte[] ib1 = TestBuffers.NewTestBuffer(PAYLOAD_SALT_A_LEN, 0xbc);
            byte[] ib2 = TestBuffers.NewTestBuffer(KEYSIZE_BYTES, 0xcd);
            byte[] ib3 = TestBuffers.NewTestBuffer(BLOCKSIZE_BYTES, 0xa0);
            byte[] ib4 = TestBuffers.NewTestBuffer(MESSAGESIZE_BYTES, 0xc2);
            byte[] ib5 = TestBuffers.NewTestBuffer(PAYLOAD_SALT_B_LEN, 0xd3);

            Encrypter encrypter = new Encrypter();
            byte[] ibAll = encrypter.AssemblePayload(ib1, ib2, ib3, ib4, ib5);

            Assert.True(ibAll.Length == PAYLOAD_SALT_A_LEN + KEYSIZE_BYTES + BLOCKSIZE_BYTES + MESSAGESIZE_BYTES + PAYLOAD_SALT_B_LEN);


            byte[] ibTest1, ibTest2, ibTest3, ibTest4, ibTest5;

            encrypter.DisassemblePayload(ibAll, out ibTest1, out ibTest2, out ibTest3, out ibTest4, out ibTest5);

            Assert.True(TestBuffers.AreEqual(ibTest1, ib1));
            Assert.True(TestBuffers.AreEqual(ibTest2, ib2));
            Assert.True(TestBuffers.AreEqual(ibTest3, ib3));
            Assert.True(TestBuffers.AreEqual(ibTest4, ib4));
            Assert.True(TestBuffers.AreEqual(ibTest5, ib5));
        }

        [Fact]
        public void BufferRoundTripWorks()
        {
            const byte PADDED_SIZE = 5 * BLOCKSIZE_BYTES;

            string password = "alongpassword";
            byte[] ibPadded = TestBuffers.NewTestBuffer(PADDED_SIZE, 0x06);

            Encrypter encrypter = new Encrypter();
            byte[] ibCypher = encrypter.EncryptBuffer(ibPadded, password);
            byte[] ibDecrypted = encrypter.DecryptBuffer(ibCypher, password);

            Assert.True(TestBuffers.AreEqual(ibDecrypted, ibPadded));
        }

        [Fact]
        public void StringRoundTripWorks()
        {
            string message = "some text";
            string password = "apassword";

            Encrypter encrypter = new Encrypter();
            byte[] ibCipherText = encrypter.Encrypt(message, password);

            string backWorks = encrypter.Decrypt(ibCipherText, password);
            Assert.True(backWorks == message);
        }

        [Fact]
        public void WrongPasswordThrows()
        {
            string message = "some text";
            string longpassword = "alongpassword";
            string password = longpassword.Substring(0, 8);

            Debug.WriteLine("Encrypting with password: '{0}'", password);
            Encrypter encrypter = new Encrypter();
            byte[] ibCipherText = encrypter.Encrypt(message, password);

            for (int n = longpassword.Length - 1; n >= 0; --n)
            {
                string str = longpassword.Substring(0, n);
                if (str != password)
                {
                    Debug.WriteLine("Checking decrypting with password: '{0}'", str);

                    try
                    {
                        string decrypt = encrypter.Decrypt(ibCipherText, str);
                        Assert.True(false);
                    }
                    catch (Exception)
                    {
                        Assert.True(true);
                    }
                }
            }
        }
    }
}
