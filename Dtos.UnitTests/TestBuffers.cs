﻿namespace DtosUnitTests
{
    public static class TestBuffers
    {
        public static byte[] NewTestBuffer(byte length, byte bStart)
        {
            //DataWriter dw = new DataWriter();
            //for (byte b = 0; b < length; ++b)
            //{
            //    dw.WriteByte((byte)(bStart + b));
            //}
            //IBuffer ib = dw.DetachBuffer();

            var ib = new byte[length];
            for (int i = 0; i < length; ++i)
            {
                ib[i] = (byte)(bStart + i);
            }

            return ib;
        }

        public static bool AreEqual(byte[] ib1, byte[] ib2)
        {
            if (ib1.Length != ib2.Length)
            {
                return false;
            }

            //DataReader dr1 = DataReader.FromBuffer(ib1);
            //DataReader dr2 = DataReader.FromBuffer(ib2);
            //for (uint u = 0; u < ib1.Length; ++u)
            //{
            //    byte b1 = dr1.ReadByte();
            //    byte b2 = dr2.ReadByte();
            //    if (b1 != b2)
            //    {
            //        return false;
            //    }
            //}

            for (int i = 0; i < ib1.Length; ++i)
            {
                if (ib1[i] != ib2[i])
                {
                    return false;
                }
            }

            return true;
        }
    }
}
