﻿using Dtos;
using Xunit;

namespace DtosUnitTests
{
    public class BoxSerializerTests
    {
        [Fact]
        public void SerializerRoundTripWorks()
        {
            Box box = TestBoxes.NewTestBox();

            IBoxSerializer serializer = new BoxSerializer();
            string serialized = serializer.Serialize(box);
            Box test = serializer.Deserialize(serialized);

            Assert.True(TestBoxes.AreEqual(box, test));
        }
    }
}
