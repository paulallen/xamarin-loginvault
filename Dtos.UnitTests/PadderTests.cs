﻿using Dtos;
using Xunit;

namespace DtosUnitTests
{
    public class PadderTests
    {
        [Fact]
        public void PadderSizeCalculationCorrect()
        {
            Padder paddy = new Padder();

            const byte BLOCKSIZE = 16;
            int uLength;

            // < BLOCKSIZE
            for (int u = 0; u < BLOCKSIZE; ++u)
            {
                uLength = paddy.PaddedBufferLength(u, BLOCKSIZE);
                Assert.True(uLength == BLOCKSIZE);
            }

            // BLOCKSIZE
            uLength = paddy.PaddedBufferLength(BLOCKSIZE, BLOCKSIZE);
            Assert.True(uLength == 2 * BLOCKSIZE);
        }

        [Fact]
        public void PadderAddWorks()
        {
            Padder paddy = new Padder();

            const byte BLOCKSIZE = 16;

            // < BLOCKSIZE
            for (byte nBytes = 1; nBytes < BLOCKSIZE; ++nBytes)
            {
                byte[] ibMsg = TestBuffers.NewTestBuffer(nBytes, 0xc0);

                byte[] ibPadded = paddy.Add(ibMsg, BLOCKSIZE);

                Assert.True(ibPadded.Length == BLOCKSIZE);

                //DataReader dr = DataReader.FromBuffer(ibPadded);
                //for (byte b = 0; b < nBytes; ++b)
                //{
                //    byte b1 = dr.ReadByte();
                //    Assert.True(b1 == 0xc0 + b);
                //}

                for (int i = 0; i < nBytes; ++i)
                {
                    Assert.True(ibPadded[i] == 0xc0 + i);
                }

                byte bTest = (byte)(BLOCKSIZE - nBytes);
                for (uint u = nBytes; u < BLOCKSIZE - 1; ++u)
                {
                    //byte bPad = dr.ReadByte();
                    Assert.True(ibPadded[u] == bTest);
                }
            }

            // BLOCKSIZE
            {
                byte[] ibMsg = TestBuffers.NewTestBuffer(BLOCKSIZE, 0xd0);

                byte[] ibPadded = paddy.Add(ibMsg, BLOCKSIZE);

                Assert.True(ibPadded.Length == 2 * BLOCKSIZE);

                //DataReader dr = DataReader.FromBuffer(ibPadded);
                //for (byte b = 0; b < BLOCKSIZE; ++b)
                //{
                //    byte b1 = dr.ReadByte();
                //    Assert.True(b1 == 0xd0 + b);
                //}

                for (int i = 0; i < BLOCKSIZE; ++i)
                {
                    Assert.True(ibPadded[i] == 0xd0 + i);
                }

                for (uint u = BLOCKSIZE; u < 2 * BLOCKSIZE; ++u)
                {
                    //byte bPad = dr.ReadByte();
                    Assert.True(ibPadded[u] == BLOCKSIZE);
                }
            }
        }

        [Fact]
        public void PadderRoundTripWorks()
        {
            Padder paddy = new Padder();

            const byte BLOCKSIZE = 16;

            // < BLOCKSIZE
            for (byte nBytes = 1; nBytes < BLOCKSIZE; ++nBytes)
            {
                byte[] ibMsg = TestBuffers.NewTestBuffer(nBytes, 0xa0);

                byte[] ibPadded = paddy.Add(ibMsg, BLOCKSIZE);
                byte[] ibStripped = paddy.Remove(ibPadded, BLOCKSIZE);

                Assert.True(ibStripped.Length == nBytes);
                Assert.True(TestBuffers.AreEqual(ibStripped, ibMsg));
            }

            // BLOCKSIZE
            {
                byte[] ibMsg = TestBuffers.NewTestBuffer(BLOCKSIZE, 0xb0);

                byte[] ibPadded = paddy.Add(ibMsg, BLOCKSIZE);
                byte[] ibStripped = paddy.Remove(ibPadded, BLOCKSIZE);

                Assert.True(ibStripped.Length == BLOCKSIZE);
                Assert.True(TestBuffers.AreEqual(ibStripped, ibMsg));
            }
        }
    }
}
