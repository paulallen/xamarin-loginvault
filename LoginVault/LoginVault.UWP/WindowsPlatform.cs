﻿using Windows.Storage;

[assembly: Xamarin.Forms.Dependency(typeof(LoginVault.UWP.WindowsPlatform))]
namespace LoginVault.UWP
{
    public class WindowsPlatform : IMyPlatform
    {
        public string GetStorageFolderPath()
        {
            return KnownFolders.DocumentsLibrary.Path;
        }
    }
}
