﻿namespace LoginVault
{
    public interface IMyPlatform
    {
        string GetStorageFolderPath();
    }
}
