﻿using System;

namespace LoginVault.Services
{
    public static class UrlCheckService
    {
        public static bool IsValidUrl(string url)
        {
            //url = url.ToLower();
            //if (url.StartsWith("https://")) { url = url.Substring(8); }
            //else if (url.StartsWith("http://")) { url = url.Substring(7); }

            //var type = Uri.CheckHostName(url);
            //return type == UriHostNameType.Dns || type == UriHostNameType.Basic;

            return url.Trim() != "";
        }
    }
}
