﻿using System.Threading.Tasks;
using LoginVault.Models;
using Utils;

namespace LoginVault.Services
{
    public class AutoSaveService
    {
        private readonly StorageService _storageService;
        private readonly Vault _lastSavedVault;

        public AutoSaveService(StorageService storage)
        {
            Logger.I($"AutoSaveService(vault: {storage.Vault.Name}, file: {storage.FileName})");

            _storageService = storage;
            _lastSavedVault = new Vault(storage.Vault);
        }

        public async Task AutoSave()
        {
            if (!_lastSavedVault.IsSameAs(_storageService.Vault))
            {
                Logger.D($"AutoSave() - saving");
                await _storageService.Write();
                _lastSavedVault.Clone(_storageService.Vault);
            }
            else
            {
                Logger.D($"AutoSave() - no changes");
            }
        }
    }
}
