﻿using StandardStorage;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using LoginVault.Models;
using Utils;
using Xamarin.Forms;
using System;
using System.IO;
using System.Collections.ObjectModel;
using System.Runtime.CompilerServices;
using LoginVault.ModelSerialization;

namespace LoginVault.Services
{
    public class StorageService
    {
        public const string FolderName = "LoginVault";
        public const string Ext = ".lv1";

        public Vault Vault { get; }
        public string Password { get; }
        public string FileName => $"{Vault.Name}{Ext}";

        public StorageService(Vault vault, string password)
        {
            Logger.I($"StorageService(vault: {vault.Name}, password: *****)");

            Vault = vault;
            Password = password;
        }

        public async Task Write()
        {
            if (Vault.Name == "")
            {
                Logger.W($"{Name()} No Vault.Name");
                return;
            }

            Logger.I($"Writing file: {FileName}");

            var loader = new ModelLoader();
            var folder = await GetDataFolder();
            var file = await folder.CreateFileAsync(FileName, CreationCollisionOption.ReplaceExisting);
            await loader.Save(Vault, file, Password);
            Vault.NotChanged();
        }

        public async Task Read()
        {
            if (Vault.Name == "")
            {
                Logger.W($"{Name()} No Vault.Name");
                return;
            }

            Logger.I($"Reading file: {FileName}");

            var loader = new ModelLoader();
            var folder = await GetDataFolder();
            var file = await folder.GetFileAsync(FileName);
            var v = await loader.Load(file, Password);
            v.Name = Vault.Name; // quick fix
            v.IsChanged = false;

            Vault.Clone(v);
            Assert.IsTrue(Vault.IsChanged == false, "Unclean!");
        }

        public static async Task<ObservableCollection<string>> GetVaultList()
        {
            try
            {
                var cancel = new CancellationToken();
                var folder = await GetDataFolder();
                var files = await folder.GetFilesAsync(cancel);
                Logger.I($"Read vault list from: {folder.Path}");

                var list = files.Where(_ => _.Name.EndsWith(Ext)).Select(_ => _.Name.Remove(_.Name.IndexOf(Ext))).ToList();
                return new ObservableCollection<string>(list);
            }
            catch (Exception ex)
            {
                Logger.X(Name(), ex);
                return new ObservableCollection<string>();
            }
        }

        public static async Task DeleteVault(string name)
        {
            var fileName = $"{name}{Ext}";

            Logger.I($"Deleting file: {fileName}");

            var cancel = new CancellationToken();
            var folder = await GetDataFolder();
            var file = await folder.GetFileAsync(fileName, cancel);
            await file.DeleteAsync();
        }

        private static async Task<IFolder> GetDataFolder()
        {
            var root = GetRootFolderPath();
            var rootFolder = await FileSystem.Current.GetFolderFromPathAsync(root);
            var dataFolder = await rootFolder.CreateFolderAsync(FolderName, CreationCollisionOption.OpenIfExists);
            return dataFolder;
        }

        private static string GetDataFolderPath()
        {
            var root = GetRootFolderPath();
            string path = Path.Combine(root, FolderName);

            Logger.D($"{Name()} - data folder: {path}");
            return path;
        }

        private static string GetRootFolderPath()
        {
            var path = DependencyService.Get<IMyPlatform>().GetStorageFolderPath();
            if (string.IsNullOrEmpty(path))
            {
                path = FileSystem.Current.LocalStorage.Path;
            }

            return path;
        }

        private static string Name([CallerMemberName] string name = "")
        {
            return name;
        }
    }
}
