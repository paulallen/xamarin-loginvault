﻿using System.Collections.Generic;
using System.Windows.Input;
using LoginVault.Commands;
using LoginVault.Mvvm;
using Utils;

namespace LoginVault.Services
{
    public class CommandService
    {
        public ICommand CmdUndo => new RelayCommand(() => Undo());
        public ICommand CmdRedo => new RelayCommand(() => Redo());

        public CommandService()
        {
            _commands = new List<BaseCommand>();
            _nextIndex = 0;
        }

        public void Clear()
        {
            _commands.Clear();
            _nextIndex = 0;
        }

        public void Do(BaseCommand cmd)
        {
            cmd.Do();

            while (_nextIndex < _commands.Count)
            {
                _commands.RemoveAt(_commands.Count - 1);
            }

            _commands.Add(cmd);
            _nextIndex = _commands.Count;
        }

        public void Undo()
        {
            if (_nextIndex == 0)
            {
                Logger.D($"Undo() - end.");
                return;
            }

            --_nextIndex;

            BaseCommand cmd = _commands[_nextIndex];
            cmd.Undo();
        }

        public void Redo()
        {
            if (_nextIndex == _commands.Count)
            {
                Logger.D($"Redo() - end.");
                return;
            }

            BaseCommand cmd = _commands[_nextIndex];
            cmd.Do();

            ++_nextIndex;
        }

        private readonly IList<BaseCommand> _commands;
        private int _nextIndex;
    }
}
