﻿using LoginVault.ViewModels;
using System;
using Utils;

namespace LoginVault.Commands
{
    public class RenameFolder : BaseCommand
    {
        public RenameFolder(AppVm appVm, int folderIndex, string name)
            : base(appVm)
        {
            _folderIndex = folderIndex;
            _newName = name;
        }

        public override void Do()
        {
            Logger.D($"Do() - change folder #{_folderIndex} name to: {_newName}");

            var folder = AppVm.CurrentVault.Folders[_folderIndex];
            Logger.D($"Do() - folder #{folder}");

            _oldName = folder.Name;
            folder.Name = _newName;
        }

        public override void Undo()
        {
            Logger.D($"Undo() - change folder #{_folderIndex} name back to: {_oldName}");

            var folder = AppVm.CurrentVault.Folders[_folderIndex];
            Logger.D($"Undo() - folder #{folder}");

            folder.Name = _oldName;
        }

        private readonly int _folderIndex;
        private readonly string _newName;
        private string _oldName;
    }
}
