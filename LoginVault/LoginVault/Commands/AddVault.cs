﻿using LoginVault.Models;
using LoginVault.ViewModels;
using Utils;

namespace LoginVault.Commands
{
    public class AddVault : BaseCommand
    {
        public AddVault(AppVm appVm, Vault vault)
            : base(appVm)
        {
            _vault = vault;
        }

        public override void Do(AppVm appVm)
        {
            Logger.D($"Do() - add vault: {_vault.Name}");

            if (!appVm.VaultNames.Contains(_vault.Name))
            {
                appVm.VaultNames.Add(_vault.Name);
                appVm.SelectedVaultName = _vault.Name;
            }
        }

        public override void Undo(AppVm appVm)
        {
            Logger.D($"Undo() - remove last vault");

            int vaultIndex = appVm.VaultNames.Count - 1;
            appVm.VaultNames.RemoveAt(vaultIndex);
        }

        private readonly Vault _vault;
    }
}
