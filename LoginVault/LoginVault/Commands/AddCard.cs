﻿using LoginVault.Models;
using LoginVault.ViewModels;
using System;
using Utils;

namespace LoginVault.Commands
{
    public class AddCard : BaseCommand
    {
        public AddCard(AppVm appVm, int folderIndex, Card card)
            : base(appVm)
        {
            Logger.D($"AddCard() - card: {card}");

            _folderIndex = folderIndex;
            _card = card;   //  new Card(card);

            Logger.D($"AddCard() - _card: {_card}");
        }

        public override void Do()
        {
            Logger.D($"Do() - in folder #{_folderIndex} add card #{_card.GetHashCode()}");

            Folder folder = AppVm.CurrentVault.Folders[_folderIndex];
            Logger.D($"Do() - folder: {folder}");

            int cardIndex = folder.Cards.Count;
            folder.Cards.Insert(cardIndex, _card);
        }

        public override void Undo()
        {
            Logger.D($"Undo() - in folder #{_folderIndex} remove last card");

            Folder folder = AppVm.CurrentVault.Folders[_folderIndex];
            Logger.D($"Undo() - folder: {folder}");

            int cardIndex = folder.Cards.Count - 1;
            folder.Cards.RemoveAt(cardIndex);
        }

        private readonly int _folderIndex;
        private readonly Card _card;
    }
}
