﻿using LoginVault.Models;
using LoginVault.ViewModels;
using System;
using Utils;

namespace LoginVault.Commands
{
    public class DeleteCard : BaseCommand
    {
        public DeleteCard(AppVm appVm, int folderIndex, int cardIndex)
            : base(appVm)
        {
            Logger.D($"DeleteCard(folderIndex: {folderIndex}, cardIndex: {cardIndex})");

            _folderIndex = folderIndex;
            _cardIndex = cardIndex;
        }

        public override void Do()
        {
            Folder folder = AppVm.CurrentVault.Folders[_folderIndex];
            Card card = folder.Cards[_cardIndex];

            Logger.D($"Do() - in folder {folder} remove card {card}");

            _oldCard = new Card(card);

            // Delete
            AppVm.CurrentVault.Folders[_folderIndex].Cards.RemoveAt(_cardIndex);

            // Put it in Backups
            Folder backupsFolder = AppVm.CurrentVault.GetBackupsFolder();
            if (folder != backupsFolder)
            {
                backupsFolder.Cards.Add(new Card(_oldCard));
            }
        }

        public override void Undo()
        {
            Logger.D($"Undo() - in folder #{_folderIndex} return card #{_cardIndex}");

            // Undelete
            Folder folder = AppVm.CurrentVault.Folders[_folderIndex];
            folder.Cards.Insert(_cardIndex, _oldCard);

            // Remove from Backups
            Folder backupsFolder = AppVm.CurrentVault.GetBackupsFolder();
            if (folder != backupsFolder)
            {
                backupsFolder.Cards.RemoveAt(backupsFolder.Cards.Count - 1);
            }
        }

        private readonly int _folderIndex;
        private readonly int _cardIndex;
        private Card _oldCard;
    }
}
