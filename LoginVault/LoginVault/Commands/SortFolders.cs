﻿using LoginVault.Models;
using LoginVault.ViewModels;
using System;
using Utils;

namespace LoginVault.Commands
{
    public class SortFolders : BaseCommand
    {
        public SortFolders(AppVm appVm)
            : base(appVm)
        { }

        public override void Do()
        {
            Logger.D($"Do() - sort folders");

            _oldCurrentFolder = AppVm.CurrentFolder;
            _oldVault.Clone(AppVm.CurrentVault);

            foreach (Folder folder in AppVm.CurrentVault.Folders)
            {
                Logger.D($"Do() - pre-sort folder #{folder}");
            }

            AppVm.CurrentVault.Folders.Sort();

            foreach (Folder folder in AppVm.CurrentVault.Folders)
            {
                Logger.D($"Do() - post-sort folder #{folder}");
            }

            RestoreCurrentFolder(AppVm);
        }

        public override void Undo()
        {
            Logger.D($"Undo() - jumble folders");

            AppVm.CurrentVault.Clone(_oldVault);

            RestoreCurrentFolder(AppVm);
        }

        private void RestoreCurrentFolder(AppVm appVm)
        {
            appVm.CurrentFolder = null;
            appVm.CurrentFolder = _oldCurrentFolder;
        }

        private Vault _oldVault;
        private Folder _oldCurrentFolder;
    }
}
