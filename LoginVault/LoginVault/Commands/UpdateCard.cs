﻿using LoginVault.Models;
using LoginVault.ViewModels;
using System;
using Utils;

namespace LoginVault.Commands
{
    public class UpdateCard : BaseCommand
    {
        public UpdateCard(AppVm appVm, int folderIndex, int cardIndex, Card newCard)
            : base(appVm)
        {
            _folderIndex = folderIndex;
            _cardIndex = cardIndex;
            _newCard = new Card(newCard);
        }

        public override void Do()
        {
            Logger.D($"Do() - update folder #{_folderIndex}, card #{_cardIndex}, newCard: {_newCard}");

            Folder folder = AppVm.CurrentVault.Folders[_folderIndex];
            Logger.D($"Do() - folder: {folder}");

            Card card = folder.Cards[_cardIndex];
            Logger.D($"Do() - card: {card}");

            _oldCard = new Card(card);
            card.Clone(_newCard);
        }

        public override void Undo()
        {
            Logger.D($"Undo() - revert folder #{_folderIndex}, card #{_cardIndex}, newCard: {_newCard}");

            Folder folder = AppVm.CurrentVault.Folders[_folderIndex];
            Logger.D($"Do() - folder: {folder}");

            Card card = folder.Cards[_cardIndex];
            Logger.D($"revert card: {card}");

            card.Clone(_oldCard);
        }

        private readonly int _folderIndex;
        private readonly int _cardIndex;
        private readonly Card _newCard;
        private Card _oldCard;
    }
}
