﻿using LoginVault.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using Utils;

namespace LoginVault.Commands
{
    class Sequence : BaseCommand
    {
        public Sequence(AppVm appVm, params BaseCommand[] commands)
            : base(appVm)
        {
            _commands = new List<BaseCommand>(commands);
        }

        public override void Do()
        {
            Logger.D($"Do() - invoking multiple...");

            foreach (BaseCommand cmd in _commands)
            {
                cmd.Do();
            }
        }

        public override void Undo()
        {
            Logger.D($"Undo() - revoking multiple...");

            foreach (BaseCommand cmd in _commands.Reverse())
            {
                cmd.Undo();
            }
        }

        private readonly IList<BaseCommand> _commands;
    }
}
