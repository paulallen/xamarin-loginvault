﻿using LoginVault.Models;
using LoginVault.ViewModels;
using System;
using Utils;

namespace LoginVault.Commands
{
    public class AddBackupsFolder : BaseCommand
    {
        public AddBackupsFolder(AppVm appVm)
            : base(appVm) { }

        public override void Do()
        {
            Logger.D($"Do() - create backups folder");

            _createDeletedFolder = AppVm.CurrentVault.GetBackupsFolder() == null;
            if (_createDeletedFolder)
            {
                AppVm.CurrentVault.CreateBackupsFolder();
            }
        }

        public override void Undo()
        {
            Logger.D($"Undo() - delete backups folder");

            if (_createDeletedFolder)
            {
                AppVm.CurrentVault.RemoveBackupsFolder();
            }
        }

        private bool _createDeletedFolder;
    }
}
