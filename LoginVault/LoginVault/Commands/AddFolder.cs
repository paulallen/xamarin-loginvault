﻿using LoginVault.Models;
using LoginVault.ViewModels;
using System;
using Utils;

namespace LoginVault.Commands
{
    public class AddFolder : BaseCommand
    {
        public AddFolder(AppVm appVm, Folder folder)
            : base(appVm)
        {
            Logger.D($"AddFolder() - folder: {folder}");

            _folder = folder;
            Logger.D($"AddFolder() - _folder: {_folder}");
        }

        public override void Do()
        {
            Logger.D($"Do() - add folder: {_folder}");

            int folderIndex = AppVm.CurrentVault.Folders.Count;
            AppVm.CurrentVault.Folders.Insert(folderIndex, _folder);
        }

        public override void Undo()
        {
            Logger.D($"Undo() - remove last folder");

            int folderIndex = AppVm.CurrentVault.Folders.Count - 1;
            AppVm.CurrentVault.Folders.RemoveAt(folderIndex);
        }

        private readonly Folder _folder;
    }
}
