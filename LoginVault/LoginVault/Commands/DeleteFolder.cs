﻿using LoginVault.Models;
using LoginVault.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using Utils;

namespace LoginVault.Commands
{
    public class DeleteFolder : BaseCommand
    {
        public DeleteFolder(AppVm appVm, int folderIndex)
            : base(appVm)
        {
            Logger.D($"DeleteFolder(folderIndex: {folderIndex})");

            _folderIndex = folderIndex;
            _deletedCardsIndexes = new List<int>();
        }

        public override void Do()
        {
            Folder folder = AppVm.CurrentVault.Folders[_folderIndex];
            Logger.D($"Do() - remove folder {folder}");

            _oldFolder = new Folder(folder);
            _deletedCardsIndexes.Clear();

            // Delete
            AppVm.CurrentVault.Folders.RemoveAt(_folderIndex);

            // Put cards in Backups, if we haven't just deleted it.
            Folder backupsFolder = AppVm.CurrentVault.GetBackupsFolder();
            if (backupsFolder != null)
            {
                if (_oldFolder != backupsFolder)
                {
                    foreach (Card card in _oldFolder.Cards)
                    {
                        int indexInDeleted = backupsFolder.Cards.Count;
                        Logger.D($"Copying card: {card} to deleted folder as card #{indexInDeleted}");

                        _deletedCardsIndexes.Add(indexInDeleted);
                        backupsFolder.Cards.Insert(indexInDeleted, new Card(card));
                    }
                }
            }
        }

        public override void Undo()
        {
            Logger.D($"Undo() - return folder: {_oldFolder} to #{_folderIndex}");

            // Undelete
            AppVm.CurrentVault.Folders.Insert(_folderIndex, _oldFolder);

            // Remove cards from Backups
            Folder backupsFolder = AppVm.CurrentVault.GetBackupsFolder();
            if (backupsFolder != null)
            {
                foreach (int indexInDeleted in _deletedCardsIndexes.Reverse())
                {
                    Logger.D($"Removing card #{indexInDeleted} in deleted");
                    backupsFolder.Cards.RemoveAt(indexInDeleted);
                }
            }
        }

        private readonly int _folderIndex;
        private readonly IList<int> _deletedCardsIndexes;
        private Folder _oldFolder;
    }
}
