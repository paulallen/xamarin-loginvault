﻿using LoginVault.Models;
using LoginVault.ViewModels;
using System;
using Utils;

namespace LoginVault.Commands
{
    public class BackupFolder : BaseCommand
    {
        public BackupFolder(AppVm appVm, int folderIndex)
            : base(appVm)
        {
            Logger.D($"BackupFolder(folderIndex: {folderIndex})");

            _folderIndex = folderIndex;
        }

        public override void Do()
        {
            Folder folder = AppVm.CurrentVault.Folders[_folderIndex];
            Logger.D($"Do() - backup folder {folder}");

            _oldFolder = new Folder(folder);
        }

        public override void Undo()
        {
            Logger.D($"Undo() - restore folder #{_folderIndex}");

            Folder folder = AppVm.CurrentVault.Folders[_folderIndex];
            folder.Clone(_oldFolder);
        }

        private readonly int _folderIndex;
        private Folder _oldFolder;
    }
}
