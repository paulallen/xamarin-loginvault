﻿using LoginVault.Models;
using LoginVault.ViewModels;
using System;
using Utils;

namespace LoginVault.Commands
{
    public class BackupCard : BaseCommand
    {
        public BackupCard(AppVm appVm, int folderIndex, int cardIndex)
            : base(appVm)
        {
            Logger.D($"BackupCard(folderIndex: {folderIndex}, cardIndex: {cardIndex})");

            _folderIndex = folderIndex;
            _cardIndex = cardIndex;
        }

        public override void Do()
        {
            Folder folder = AppVm.CurrentVault.Folders[_folderIndex];
            Card card = folder.Cards[_cardIndex];

            Logger.D($"Do() - in folder {folder}, backup card {card}");
            _oldCard = new Card(card);
        }

        public override void Undo()
        {
            Logger.D($"Undo() - in folder #{_folderIndex}, restore card #{_cardIndex}");

            Card card = AppVm.CurrentVault.Folders[_folderIndex].Cards[_cardIndex];
            card.Clone(_oldCard);
        }

        private readonly int _folderIndex;
        private readonly int _cardIndex;
        private Card _oldCard;
    }
}
