﻿using LoginVault.Models;
using LoginVault.ViewModels;
using System;
using Utils;

namespace LoginVault.Commands
{
    public class SortCards : BaseCommand
    {
        public SortCards(AppVm appVm, int folderIndex)
            : base(appVm)
        {
            _folderIndex = folderIndex;
        }

        public override void Do()
        {
            Logger.D($"Do() - in folder #{_folderIndex} sort the cards");

            _oldCurrentCard = AppVm.CurrentCard;

            Folder folder = AppVm.CurrentVault.Folders[_folderIndex];
            Logger.D($"Do() - folder {folder}");

            foreach (Card card in folder.Cards)
            {
                Logger.D($"Do() - pre-sort card #{card}");
            }

            _oldFolder = new Folder(folder);

            folder.Cards.Sort();

            foreach (Card card in folder.Cards)
            {
                Logger.D($"Do() - post-sort card #{card}");
            }

            RestoreCurrentCard(AppVm);
        }

        public override void Undo()
        {
            Logger.D($"Undo() - in folder #{_folderIndex} jumble the cards");

            Folder folder = AppVm.CurrentVault.Folders[_folderIndex];
            Logger.D($"Undo() - folder {folder}");

            folder.Clone(_oldFolder);

            RestoreCurrentCard(AppVm);
        }

        private void RestoreCurrentCard(AppVm appVm)
        {
            appVm.CurrentCard = null;
            appVm.CurrentCard = _oldCurrentCard;
        }

        private readonly int _folderIndex;
        private Folder _oldFolder;
        private Card _oldCurrentCard;
    }
}
