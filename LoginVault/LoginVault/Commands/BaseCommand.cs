﻿using LoginVault.ViewModels;

namespace LoginVault.Commands
{
    public abstract class BaseCommand
    {
        public readonly AppVm AppVm;
        public BaseCommand(AppVm appVm) { AppVm = appVm; }
        public virtual void Do() { }
        public virtual void Undo() { }
    }
}
