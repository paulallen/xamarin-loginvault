﻿using System.IO;
using System.Threading.Tasks;

namespace LoginVault.ModelSerialization
{
    internal interface IMyStreamWriter
    {
        Task Write(Stream stream, string payload);
        Task<string> Read(Stream stream);
    }
}
