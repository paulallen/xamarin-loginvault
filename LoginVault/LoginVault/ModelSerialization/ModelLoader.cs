﻿using LoginVault.DtoSerialization;
using LoginVault.Models;
using StandardStorage;
using System.Threading.Tasks;
using static PCLCrypto.WinRTCrypto;

namespace LoginVault.ModelSerialization
{
    public class ModelLoader : IModelLoader
    {
        IMapper mapper = new Mapper();
        IBoxSerializer serializer = new BoxSerializer();
        IEncrypter encryptor = new Encrypter();
        IMyStreamWriter writer = new MyStreamWriter();

        public async Task<Vault> Load(IFile file, string password)
        {
            using (var stream = await file.OpenAsync(FileAccess.Read))
            {
                var payload = await writer.Read(stream);
                var ibCipher = CryptographicBuffer.DecodeFromBase64String(payload);
                var decoded = encryptor.Decrypt(ibCipher, password);
                var box = serializer.Deserialize(decoded);
                var vault = mapper.FromDto(box);

                return vault;
            }
        }

        public async Task Save(Vault vault, IFile file, string password)
        {
            using (var stream = await file.OpenAsync(FileAccess.ReadAndWrite))
            {
                var box = mapper.ToDto(vault);
                var raw = serializer.Serialize(box);
                var ibCipher = encryptor.Encrypt(raw, password);
                string payload = CryptographicBuffer.EncodeToBase64String(ibCipher);

                await writer.Write(stream, payload);
            }
        }
    }
}
