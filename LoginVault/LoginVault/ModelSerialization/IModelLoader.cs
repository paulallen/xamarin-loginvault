﻿using LoginVault.Models;
using StandardStorage;
using System.Threading.Tasks;

namespace LoginVault.ModelSerialization
{
    public interface IModelLoader
    {
        Task<Vault> Load(IFile file, string password);
        Task Save(Vault vault, IFile file, string password);
    }
}
