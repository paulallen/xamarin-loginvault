﻿using LoginVault.Models;

namespace LoginVault.ModelSerialization
{
    public interface IMapper
    {
        Dtos.Box ToDto(Vault vault);
        Vault FromDto(Dtos.Box box);
    }
}
