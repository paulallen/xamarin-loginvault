﻿using System;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace LoginVault.ModelSerialization
{
    internal class MyStreamWriter : IMyStreamWriter
    {
        public async Task Write(Stream stream, string payload)
        {
            using (StreamWriter sw = new StreamWriter(stream))
            {
                await sw.WriteLineAsync(HEADER);

                int nLength = payload.Length;
                for (int nIndex = 0; nIndex < nLength; nIndex += 64)
                {
                    int nBlockSize = 64;
                    if (nLength - nIndex < nBlockSize)
                    {
                        nBlockSize = nLength - nIndex;
                    }

                    string strLine = payload.Substring(nIndex, nBlockSize);
                    await sw.WriteLineAsync(strLine);
                }

                await sw.WriteLineAsync(FOOTER);

                sw.Flush();
            }
        }

        public async Task<string> Read(Stream stream)
        {
            using (StreamReader sr = new StreamReader(stream))
            {
                string strHeader = await sr.ReadLineAsync();

                if (strHeader == HEADER)
                {
                    StringBuilder sbPayload = new StringBuilder();
                    string strBlock;
                    do
                    {
                        strBlock = await sr.ReadLineAsync();
                        if (strBlock != FOOTER)
                        {
                            if (strBlock.Length <= 64)
                            {
                                sbPayload.Append(strBlock);
                            }
                            else
                            {
                                throw new Exception("Read() - Invalid file format");
                            }
                        }
                    } while (strBlock != FOOTER);

                    return sbPayload.ToString();
                }
                else
                {
                    throw new Exception("Read() - Invalid file format");
                }
            }
        }

        private const string HEADER = "===== BEGIN LOGINVAULT V1 =====";
        private const string FOOTER = "===== END LOGINVAULT V1 =====";
    }
}
