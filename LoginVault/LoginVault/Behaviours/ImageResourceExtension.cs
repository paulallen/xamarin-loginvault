﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace LoginVault.Behaviours
{
    [ContentProperty("Source")]
    public class ImageResourceExtension : IMarkupExtension
    {
        public string Source { get; set; }

        public object ProvideValue(IServiceProvider serviceProvider)
        {
            if (Source == null) throw new InvalidOperationException("No Source");

            // Do your translation lookup here, using whatever method you require
            var imageSource = ImageSource.FromResource(Source);
            return imageSource;
        }
    }
}
