﻿using System;
using Xamarin.Forms;

namespace LoginVault.Behaviours
{
    public class UrlValidationBehaviour : Behavior<Entry>
    {
        private static readonly BindablePropertyKey IsValidPropertyKey = BindableProperty.CreateReadOnly("IsValid", typeof(bool), typeof(UrlValidationBehaviour), false);
        public static readonly BindableProperty IsValidProperty = IsValidPropertyKey.BindableProperty;

        public bool IsValid
        {
            get { return (bool)GetValue(IsValidProperty); }
            private set { SetValue(IsValidPropertyKey, value); }
        }

        protected override void OnAttachedTo(Entry bindable)
        {
            bindable.TextChanged += OnTextChanged;
        }

        protected override void OnDetachingFrom(Entry bindable)
        {
            bindable.TextChanged -= OnTextChanged;
        }

        private void OnTextChanged(object sender, TextChangedEventArgs e)
        {
            var value = e.NewTextValue.StartsWith("http://") || e.NewTextValue.StartsWith("https://") ? e.NewTextValue : $"http://{e.NewTextValue}";
            IsValid = Uri.IsWellFormedUriString(value, UriKind.Absolute);

            if (!(sender is Entry entry)) return;

            entry.TextColor = IsValid ? Color.Default : Color.Red;
        }
    }
}
