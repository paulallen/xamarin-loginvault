﻿using System;
using System.Globalization;
using Xamarin.Forms;

namespace LoginVault.Behaviours
{
    public class BoolToImageSourceConverter : IValueConverter
    {
        public ImageSource TrueImage { get; set; }
        public ImageSource FalseImage { get; set; }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (bool)value
                ? TrueImage
                : FalseImage;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
