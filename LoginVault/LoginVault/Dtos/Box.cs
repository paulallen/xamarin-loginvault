﻿using System.Collections.Generic;

namespace Dtos
{
    public class Box
    {
        public List<Folder> Folders { get; } = new List<Folder>();
    }
}
