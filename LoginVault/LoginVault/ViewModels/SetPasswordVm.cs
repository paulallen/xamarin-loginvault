﻿using LoginVault.Mvvm;
using System.Threading.Tasks;
using System.Windows.Input;

namespace LoginVault.ViewModels
{
    public class SetPasswordVm : Observable
    {
        public bool IsOk { get; private set; }

        private readonly AppVm _appVm;
        private bool _isBeforeOk = true;

        private string _vaultName;
        public string VaultName
        {
            get { return _vaultName; }
            set
            {
                SetField(ref _vaultName, value);
                InvokePropertyChanged(nameof(IsVaultNameBlank));
                InvokePropertyChanged(nameof(IsVaultNameExisting));
                InvokePropertyChanged(nameof(IsVaultNameOk));
            }
        }

        private string _password;
        public string Password
        {
            get { return _password; }
            set
            {
                SetField(ref _password, value);
                InvokePropertyChanged(nameof(IsPasswordOk));
                InvokePropertyChanged(nameof(IsRepeatPasswordOk));
            }
        }

        private string _repeatPassword;
        public string RepeatPassword
        {
            get { return _repeatPassword; }
            set
            {
                SetField(ref _repeatPassword, value);
                InvokePropertyChanged(nameof(IsRepeatPasswordOk));
            }
        }

        public bool IsVaultNameOk => _isBeforeOk ||
            (!IsVaultNameBlank && !IsVaultNameExisting);

        public bool IsVaultNameBlank => !_isBeforeOk &&
            string.IsNullOrWhiteSpace(VaultName);

        public bool IsVaultNameExisting => !_isBeforeOk &&
            _appVm.AsyncVaultList.Result.Contains(VaultName);

        public bool IsPasswordOk => _isBeforeOk || 
            !string.IsNullOrWhiteSpace(Password) && Password.Length > 3;

        public bool IsRepeatPasswordOk => _isBeforeOk 
            || !string.IsNullOrWhiteSpace(RepeatPassword) && RepeatPassword == Password;

        public ICommand CmdOk => new RelayCommand(async () => await Close());

        public SetPasswordVm(AppVm appVm)
        {
            _appVm = appVm;
            _vaultName = "";
            _password = "";
            _repeatPassword = "";
        }

        private async Task Close()
        {
            _isBeforeOk = false;

            if (IsVaultNameOk && IsPasswordOk && IsRepeatPasswordOk)
            {
                IsOk = true;
                await _appVm.Navigation.PopAsync();
                await _appVm.DoNewVault(VaultName, Password);
            }
            else
            {
                InvokePropertyChanged(nameof(IsVaultNameBlank));
                InvokePropertyChanged(nameof(IsVaultNameExisting));
                InvokePropertyChanged(nameof(IsVaultNameOk));
                InvokePropertyChanged(nameof(IsPasswordOk));
                InvokePropertyChanged(nameof(IsRepeatPasswordOk));
            }
        }
    }
}
