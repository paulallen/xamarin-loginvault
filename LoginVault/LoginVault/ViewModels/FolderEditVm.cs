﻿using LoginVault.Models;
using LoginVault.Mvvm;
using System.Threading.Tasks;
using System.Windows.Input;

namespace LoginVault.ViewModels
{
    public class FolderEditVm : Observable
    {
        public AppVm AppVm { get => _appVm; }

        public bool IsOk { get; private set; }

        private readonly AppVm _appVm;
        private bool _isBeforeOk = true;

        private string _name;
        public string Name
        {
            get { return _name; }
            set { SetField(ref _name, value); }
        }

        public bool IsFolderNameOk => _isBeforeOk ||
            Name.Trim() != "";

        public ICommand CmdOk => new RelayCommand(async () => await Close());

        public FolderEditVm(AppVm appVm, Folder folder)
        {
            _appVm = appVm;
            _name = folder.Name;
        }

        private async Task Close()
        {
            _isBeforeOk = false;

            if (IsFolderNameOk)
            {
                IsOk = true;
                await _appVm.Navigation.PopAsync();
                _appVm.DoUpdateFolder(this);
            }
            else
            {
                InvokePropertyChanged(nameof(IsFolderNameOk));
            }
        }
    }
}
