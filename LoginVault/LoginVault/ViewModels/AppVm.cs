﻿using LoginVault.Commands;
using LoginVault.Models;
using LoginVault.Mvvm;
using LoginVault.Services;
using LoginVault.Views;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows.Input;
using Utils;
using Xamarin.Forms;

namespace LoginVault.ViewModels
{
#nullable enable

    public class AppVm : Observable
    {
        public INavigation Navigation { get; }
        public AutoSaveService AutoSaveService { get; private set; }
        public StorageService StorageService { get; private set; }
        public CommandService CommandService { get; }

        private NotifyTaskCompletion<ObservableCollection<string>> _asyncVaultList;
        public NotifyTaskCompletion<ObservableCollection<string>> AsyncVaultList
        {
            get { return _asyncVaultList; }
            private set { SetField(ref _asyncVaultList, value); }
        }

        // TODO move into the views somehow
        public string RevealButtonText
        {
            get { return LayoutVm.Instance.HidePasswords ? "Reveal" : "Hide"; }
        }

        private string? _selectedVaultName;
        public string? SelectedVaultName
        {
            get { return _selectedVaultName; }
            set
            {
                SetField(ref _selectedVaultName, value);
                Logger.D($"SelectedVaultName set to: {value ?? "(null)"}");
            }
        }

        private Vault _currentVault;
        public Vault CurrentVault
        {
            get
            {
                Logger.D($"get CurrentVault() - {_currentVault}");
                return _currentVault;
            }
            set
            {
                if (CurrentVault != null)
                {
                    CurrentVault.PropertyChanged -= OnPropertyChanged;
                }

                if (value != _currentVault)
                {
                    CurrentFolder = null;
                }

                SetField(ref _currentVault, value);
                Logger.D($"set CurrentVault() - {_currentVault}");

                if (CurrentVault == null) return;

                CurrentVault.PropertyChanged += OnPropertyChanged;
            }
        }

        private Folder? _currentFolder;
        public Folder? CurrentFolder
        {
            get
            {
                Logger.D($"get CurrentFolder() - {_currentFolder}");
                return _currentFolder;
            }
            set
            {
                if (value != _currentFolder)
                {
                    CurrentCard = null;
                }

                SetField(ref _currentFolder, value);
                Logger.D($"set CurrentFolder() - {_currentFolder}");
            }
        }

        private Card? _currentCard;
        public Card? CurrentCard
        {
            get
            {
                Logger.D($"get CurrentCard() - {_currentCard}");
                return _currentCard;
            }
            set
            {
                SetField(ref _currentCard, value);
                Logger.D($"set CurrentCard() - {_currentCard}");
            }
        }

        public ICommand CmdNewVault => new RelayCommand(async () => await NewVault());
        public ICommand CmdOpenVault => new RelayCommand(async () => await OpenVault());
        public ICommand CmdDeleteVault => new RelayCommand(async () => await DeleteVault());

        public ICommand CmdNewFolder => new RelayCommand(async () => await NewFolder());
        public ICommand CmdOpenFolder => new RelayCommand(async () => await OpenFolder());
        public ICommand CmdDeleteFolder => new RelayCommand(async () => await DeleteFolder());

        public ICommand CmdNewCard => new RelayCommand(async () => await NewCard());
        public ICommand CmdOpenCard => new RelayCommand(async () => await OpenCard());
        public ICommand CmdEditCard => new RelayCommand(async () => await EditCard());
        public ICommand CmdDeleteCard => new RelayCommand(async () => await DeleteCard());

        public ICommand CmdToggleReveal => new RelayCommand(() => ToggleReveal());
        public ICommand CmdFind => new RelayCommand(async () => await Find());

        public AppVm(INavigation navigation)
        {
            Navigation = navigation;

            LayoutVm.Instance.ShowPasswords = false;

            _asyncVaultList = new NotifyTaskCompletion<ObservableCollection<string>>(Task.FromResult(new ObservableCollection<string>()));
            _currentVault = new Vault { Name = "" };

            StorageService = new StorageService(CurrentVault, "");
            AutoSaveService = new AutoSaveService(StorageService);
            CommandService = new CommandService();
        }

        #region Pages need to do things

        public void DoLoadVaultList()
        {
            try
            {
                Logger.D(Name());
                AssertValid();

                AsyncVaultList = new NotifyTaskCompletion<ObservableCollection<string>>(StorageService.GetVaultList());
            }
            catch (Exception ex)
            {
                Logger.X(Name(), ex);
            }
        }

        public void DoAutoSave()
        {
            try
            {
                if (AutoSaveService != null)
                {
                    Task _ = AutoSaveService.AutoSave();
                }
            }
            catch (Exception ex)
            {
                Logger.X(Name(), ex);
            }
        }

        public async Task DoNewVault(string name, string password)
        {
            try
            {
                Logger.D(Name());
                AssertValid();

                CurrentVault = new Vault { Name = name };
                StorageService = new StorageService(CurrentVault, password);
                AutoSaveService = new AutoSaveService(StorageService);

                await GoToVaultView();
            }
            catch (Exception ex)
            {
                Logger.X(Name(), ex);
                await ShowException(ex);
            }
        }

        public async Task DoOpenVault(string password)
        {
            try
            {
                Logger.D(Name());
                AssertValid();

                if (SelectedVaultName == null) return;

                CurrentVault = new Vault(SelectedVaultName);
                StorageService = new StorageService(CurrentVault, password);
                AutoSaveService = new AutoSaveService(StorageService);
                await StorageService.Read();

                await GoToVaultView();
            }
            catch (Exception ex)
            {
                Logger.X(Name(), ex);
                await ShowMessage("Fail", "There was a problem opening this vault.  Check the password and try again.");
            }
        }

        public async Task DoCloseVault()
        {
            try
            {
                Logger.D(Name());
                AssertValid();

                DoAutoSave();
            }
            catch (Exception ex)
            {
                Logger.X(Name(), ex);
                await ShowException(ex);
            }
        }

        public void DoUpdateFolder(FolderEditVm updatedFolder)
        {
            if (CurrentVault == null) throw new InvalidOperationException("CurrentVault");
            if (CurrentFolder == null) throw new InvalidOperationException("CurrentVault");

            try
            {
                Logger.D(Name());
                AssertValid();

                Logger.D($"DoUpdateFolder(updatedFolder: {updatedFolder.Name})");

                LogFolders("pre-update", CurrentVault);

                var folderIndex = CurrentVault.Folders.IndexOf(CurrentFolder);

                Logger.D($"DoUpdateFolder() CurrentVault: {CurrentVault})");
                Logger.D($"DoUpdateFolder() CurrentFolder: {CurrentFolder})");
                Logger.D($"DoUpdateFolder() folderIndex: {folderIndex})");

                var seq = new Sequence(this,
                    new RenameFolder(this, folderIndex, updatedFolder.Name),
                    new SortFolders(this)
                    );

                CommandService.Do(seq);

                LogFolders("post-update", CurrentVault);
            }
            catch (Exception ex)
            {
                Logger.X(Name(), ex);
            }
        }

        public IEnumerable<Card> DoFind(string text)
        {
            var list = new List<Card>();

            var lower = text.ToLower();
            foreach (var folder in CurrentVault.Folders)
            {
                var matches = folder.Cards.Where(card => CardMatches(card, text));
                foreach (var card in matches)
                {
                    list.Add(card);
                }
            }

            return list;
        }

        public async Task DoNavigateToCard(Card desiredCard)
        {
            try
            {
                Logger.D(Name());
                AssertValid();

                foreach (var folder in CurrentVault.Folders)
                {
                    foreach (var card in folder.Cards)
                    {
                        if (card == desiredCard)
                        {
                            Logger.D($"navigate to card {card}");

                            CurrentFolder = folder;
                            CurrentCard = card;

                            await Navigation.PushAsync(new CardView());
                            return;
                        }
                    }
                }

                Logger.E($"{Name()} - did not find desiredCard: {desiredCard.Url}");
            }
            catch (Exception ex)
            {
                Logger.X(Name(), ex);
                await ShowException(ex);
            }
        }

        public void DoUpdateCard(CardEditVm updatedCard)
        {
            try
            {
                Logger.D(Name());
                AssertValid();

                if (CurrentFolder == null) throw new InvalidOperationException("CurrentFolder");
                if (CurrentCard == null) throw new InvalidOperationException("CurrentCard");

                var folderIndex = CurrentVault.Folders.IndexOf(CurrentFolder);
                var cardIndex = CurrentFolder.Cards.IndexOf(CurrentCard);

                if (folderIndex < 0) throw new InvalidOperationException("folderIndex");
                if (cardIndex < 0) throw new InvalidOperationException("cardIndex");

                LogCards("pre-update", CurrentFolder);

                Card card = new Card(
                    updatedCard.Url,
                    updatedCard.Username,
                    updatedCard.Password,
                    updatedCard.Other);

                Logger.D($"update local card: {card}");

                var seq = new Sequence(this,
                    new UpdateCard(this, folderIndex, cardIndex, card),
                    new SortCards(this, folderIndex)
                    );

                CommandService.Do(seq);

                LogCards("post-update", CurrentFolder);
            }
            catch (Exception ex)
            {
                Logger.X(Name(), ex);
            }
        }

        #endregion

        private async Task GoToVaultView()
        {
            try
            {
                var view = new VaultView();
                await Navigation.PushAsync(view);
            }
            catch (Exception ex)
            {
                Logger.X(Name(), ex);
                await ShowException(ex);
            }
        }

        private async Task NewVault()
        {
            try
            {
                await DoCloseVault();

                Logger.D(Name());
                AssertValid();

                var setPasswordVm = new SetPasswordVm(this);
                var view = new SetPasswordView { BindingContext = setPasswordVm };
                await Navigation.PushAsync(view);
            }
            catch (Exception ex)
            {
                Logger.X(Name(), ex);
                await ShowException(ex);
            }
        }

        private async Task OpenVault()
        {
            try
            {
                await DoCloseVault();

                Logger.D(Name());
                AssertValid();

                if (SelectedVaultName == null)
                {
                    return;
                }

                var enterPasswordVm = new EnterPasswordVm(this) { VaultName = SelectedVaultName };
                var view = new EnterPasswordView { BindingContext = enterPasswordVm };
                await Navigation.PushAsync(view);
            }
            catch (Exception ex)
            {
                Logger.X(Name(), ex);
                await ShowException(ex);
            }
        }

        private async Task DeleteVault()
        {
            try
            {
                await DoCloseVault();

                Logger.D(Name());
                AssertValid();

                if (SelectedVaultName == null)
                {
                    return;
                }

                await StorageService.DeleteVault(SelectedVaultName);
                AsyncVaultList.Result.Remove(SelectedVaultName);
            }
            catch (Exception ex)
            {
                Logger.X(Name(), ex);
                await ShowException(ex);
            }
        }

        private async Task Find()
        {
            try
            {
                var vm = new FindVm(this);
                var view = new FindView { BindingContext = vm };
                await Navigation.PushAsync(view);
            }
            catch (Exception ex)
            {
                Logger.X(Name(), ex);
                await ShowException(ex);
            }
        }

        private async Task NewFolder()
        {
            try
            {
                Logger.D(Name());
                AssertValid();

                // Add 
                var folder = new Folder();
                Logger.D($"OnNewFolder() - new folder: {folder}");

                LogFolders("pre-add", CurrentVault);

                var cmd = new AddFolder(this, folder);
                CommandService.Do(cmd);

                LogFolders("post-add", CurrentVault);

                // Edit
                CurrentFolder = folder;
                await EditSelectedFolder();
            }
            catch (Exception ex)
            {
                Logger.X(Name(), ex);
                await ShowException(ex);
            }
        }

        private async Task RenameFolder()
        {
            try
            {
                Logger.D(Name());
                AssertValid();

                if (CurrentFolder == null) throw new InvalidOperationException("CurrentFolder");

                LogFolders("pre-rename", CurrentVault);

                var folderIndex = CurrentVault.Folders.IndexOf(CurrentFolder);
                if (folderIndex < 0)
                    return;

                // Can Undo but not Redo
                var cmd = new BackupFolder(this, folderIndex);
                CommandService.Do(cmd);

                // Edit
                await EditSelectedFolder();

                LogFolders("post-rename", CurrentVault);
            }
            catch (Exception ex)
            {
                Logger.X(Name(), ex);
                await ShowException(ex);
            }
        }

        private async Task EditSelectedFolder()
        {
            if (CurrentFolder == null) throw new InvalidOperationException("CurrentFolder");

            var vm = new FolderEditVm(this, CurrentFolder);
            var view = new FolderEdit { BindingContext = vm };
            await Navigation.PushAsync(view);
        }

        private async Task OpenFolder()
        {
            try
            {
                Logger.D(Name());
                AssertValid();

                if (CurrentFolder == null) throw new InvalidOperationException("CurrentFolder");

                var folderIndex = CurrentVault.Folders.IndexOf(CurrentFolder);
                if (folderIndex < 0)
                    return;

                await Navigation.PushAsync(new FolderView());
            }
            catch (Exception ex)
            {
                Logger.X(Name(), ex);
                await ShowException(ex);
            }
        }

        private async Task DeleteFolder()
        {
            try
            {
                Logger.D(Name());
                AssertValid();

                if (CurrentFolder == null) throw new InvalidOperationException("CurrentFolder");

                var folderIndex = CurrentVault.Folders.IndexOf(CurrentFolder);
                if (folderIndex < 0)
                    return;

                var seq = new Sequence(this,
                    new AddBackupsFolder(this),
                    new DeleteFolder(this, folderIndex));

                CommandService.Do(seq);

                CurrentFolder = null;
                AssertValid();
            }
            catch (Exception ex)
            {
                Logger.X(Name(), ex);
                await ShowException(ex);
            }
        }

        private async Task NewCard()
        {
            try
            {
                Logger.D(Name());
                AssertValid();

                if (CurrentFolder == null) throw new InvalidOperationException("CurrentFolder");

                var folderIndex = CurrentVault.Folders.IndexOf(CurrentFolder);
                if (folderIndex < 0)  throw new InvalidOperationException("folderIndex");

                LogCards("pre-add", CurrentFolder);

                // Add 
                var card = new Card();
                Logger.D($"OnNewCard() - new card: {card}");

                var cmd = new AddCard(this, folderIndex, card);
                CommandService.Do(cmd);

                LogCards("post-add", CurrentFolder);

                // Edit
                CurrentCard = card;
                await EditSelectedCard();
            }
            catch (Exception ex)
            {
                Logger.X(Name(), ex);
                await ShowException(ex);
            }
        }

        private async Task OpenCard()
        {
            try
            {
                Logger.D(Name());
                AssertValid();

                if (CurrentFolder == null) throw new InvalidOperationException("CurrentFolder");
                if (CurrentCard == null) throw new InvalidOperationException("CurrentCard");

                var folderIndex = CurrentVault.Folders.IndexOf(CurrentFolder);
                var cardIndex = CurrentFolder.Cards.IndexOf(CurrentCard);

                if (folderIndex < 0) throw new InvalidOperationException("folderIndex");
                if (cardIndex < 0) throw new InvalidOperationException("cardIndex");

                await Navigation.PushAsync(new CardView());
            }
            catch (Exception ex)
            {
                Logger.X(Name(), ex);
                await ShowException(ex);
            }
        }

        private async Task EditCard()
        {
            try
            {
                Logger.D(Name());
                AssertValid();

                if (CurrentFolder == null) throw new InvalidOperationException("CurrentFolder");
                if (CurrentCard == null) throw new InvalidOperationException("CurrentCard");

                var folderIndex = CurrentVault.Folders.IndexOf(CurrentFolder);
                var cardIndex = CurrentFolder.Cards.IndexOf(CurrentCard);

                if (folderIndex < 0) throw new InvalidOperationException("folderIndex");
                if (cardIndex < 0) throw new InvalidOperationException("cardIndex");

                LogCards("pre-edit", CurrentFolder);

                // Backup
                var cmd = new BackupCard(this, folderIndex, cardIndex);
                CommandService.Do(cmd);

                // Edit
                await EditSelectedCard();

                LogCards("post-edit", CurrentFolder);
            }
            catch (Exception ex)
            {
                Logger.X(Name(), ex);
                await ShowException(ex);
            }
        }

        private async Task EditSelectedCard()
        {
            if (CurrentCard == null) throw new InvalidOperationException("CurrentCard");

            var vm = new CardEditVm(this, CurrentCard);
            var view = new CardEdit { BindingContext = vm };
            await Navigation.PushAsync(view);
        }

        private async Task DeleteCard()
        {
            try
            {
                Logger.D(Name());
                AssertValid();

                if (CurrentFolder == null) throw new InvalidOperationException("CurrentFolder");
                if (CurrentCard == null) throw new InvalidOperationException("CurrentCard");

                var folderIndex = CurrentVault.Folders.IndexOf(CurrentFolder);
                var cardIndex = CurrentFolder.Cards.IndexOf(CurrentCard);

                if (folderIndex < 0) throw new InvalidOperationException("folderIndex");
                if (cardIndex < 0) throw new InvalidOperationException("cardIndex");

                var seq = new Sequence(this,
                    new AddBackupsFolder(this),
                    new DeleteCard(this, folderIndex, cardIndex));

                CommandService.Do(seq);

                CurrentCard = null;
                AssertValid();
            }
            catch (Exception ex)
            {
                Logger.X(Name(), ex);
                await ShowException(ex);
            }
        }

        private void AssertValid()
        {
            try
            {
                if (AsyncVaultList == null) throw new AssertionException("AsyncVaultList null");

                if (CurrentCard != null)
                {
                    if (CurrentFolder == null) throw new AssertionException("CurrentFolder null");
                    Assert.IsTrue(this.CurrentFolder.Cards.Contains(CurrentCard), "CurrentFolder Contains CurrentCard");
                }

                if (CurrentFolder != null)
                {
                    if (CurrentVault == null) throw new AssertionException("CurrentVault null");

                    if (CurrentFolder.Cards == null) throw new AssertionException("CurrentFolder.Cards null");
                    Assert.IsTrue(this.CurrentVault.Folders.Contains(CurrentFolder), "CurrentVault Contains CurrentFolder");
                }

                if (this.CurrentVault != null)
                {
                    Assert.IsTrue(this.CurrentVault.Folders != null, "CurrentVault.Folders null");
                }
            }
            catch (AssertionException) { } // Stay up
        }

        private void ToggleReveal()
        {
            LayoutVm.Instance.ShowPasswords = !LayoutVm.Instance.ShowPasswords;
        }

        private async Task ShowException(Exception ex)
        {
            await ShowMessage("Error", ex.Message);
        }

        private async Task ShowMessage(string title, string message)
        {
            await Application.Current.MainPage.DisplayAlert(title, message, "OK");
        }

        private void OnPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(CurrentVault.IsChanged))
            {
                Logger.D($"CurrentVault IsChanged: {CurrentVault.IsChanged}");
            }
        }

        private static bool CardMatches(Card card, string text)
        {
            return SafeContainsLower(card.Url, text)
                        || SafeContainsLower(card.Username, text)
                        || SafeContainsLower(card.Password, text)
                        || SafeContainsLower(card.Other, text);
        }

        private static bool SafeContainsLower(string value, string text)
        {
            return value != null && value.ToLower().Contains(text);
        }

        private void LogFolders(String prefix, Vault vault)
        {
            foreach (var f in vault.Folders)
            {
                Logger.D($"{prefix}: {f}");
            }
        }

        private void LogCards(String prefix, Folder folder)
        {
            foreach (var c in folder.Cards)
            {
                Logger.D($"{prefix}: {c}");
            }
        }

        private string Name([CallerMemberName] string name = "")
        {
            return name;
        }
    }
}
