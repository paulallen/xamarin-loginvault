﻿using LoginVault.Commands;
using LoginVault.Models;
using LoginVault.Mvvm;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows.Input;

namespace LoginVault.ViewModels
{
#nullable enable

    public class FindVm : Observable
    {
        public AppVm AppVm { get; private set; }

        public string FindText { get; set; }
        public ObservableCollection<Card> MatchingCards { get; private set; }
        public Card? CurrentCard { get; set; }

        public bool IsSearchTextOk => !string.IsNullOrWhiteSpace(FindText);

        public ICommand CmdFind => new RelayCommand(OnFind);
        public ICommand CmdOpenCard => new RelayCommand(async() => await OnOpen());

        public FindVm(AppVm appVm)
        {
            FindText = "";
            MatchingCards = new ObservableCollection<Card>();

            AppVm = appVm;
        }

        private void OnFind()
        {
            if (FindText.Trim().Length < 3)
            {
                return;
            }

            MatchingCards.Clear();
            var matches = AppVm.DoFind(FindText);
            foreach (var card in matches)
            {
                MatchingCards.Add(card);
            }
        }

        private async Task OnOpen()
        {
            if (CurrentCard == null)
            {
                return;
            }

            await AppVm.DoNavigateToCard(CurrentCard);
        }
    }
}
