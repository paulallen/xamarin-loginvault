﻿using LoginVault.Models;
using LoginVault.Mvvm;
using LoginVault.Services;
using System.Threading.Tasks;
using System.Windows.Input;

namespace LoginVault.ViewModels
{
    public class CardEditVm : Observable
    {
        public AppVm AppVm { get; }

        public bool IsOk { get; private set; }

        private bool _isBeforeOk = true;

        private string _url;
        public string Url
        {
            get { return _url; }
            set
            {
                SetField(ref _url, value);
                InvokePropertyChanged(nameof(IsUrlOk));
            }
        }

        private string _username;
        public string Username
        {
            get { return _username; }
            set { SetField(ref _username, value); }
        }

        private string _password;
        public string Password
        {
            get { return _password; }
            set
            {
                SetField(ref _password, value);
                InvokePropertyChanged(nameof(IsRepeatPasswordOk));
            }
        }

        private string _repeatPassword;
        public string RepeatPassword
        {
            get { return _repeatPassword; }
            set
            {
                SetField(ref _repeatPassword, value);
                InvokePropertyChanged(nameof(IsRepeatPasswordOk));
            }
        }

        private string _other;
        public string Other
        {
            get { return _other; }
            set { SetField(ref _other, value); }
        }

        public bool IsUrlOk => _isBeforeOk ||
            (Url.Trim() != "" && UrlCheckService.IsValidUrl(Url));

        public bool IsRepeatPasswordOk => _isBeforeOk ||
            RepeatPassword == Password;

        public ICommand CmdOk => new RelayCommand(async () => await Close());

        public CardEditVm(AppVm appVm, Card card)
        {
            AppVm = appVm;

            _url = card.Url;
            _username = card.Username;
            _password = card.Password;
            _repeatPassword = card.Password;
            _other = card.Other;
        }

        private async Task Close()
        {
            _isBeforeOk = false;

            if (IsUrlOk && LayoutVm.Instance.HidePasswords && RepeatPassword == Password)
            {
                IsOk = true;
                await AppVm.Navigation.PopAsync();
                AppVm.DoUpdateCard(this);
            }
            else
            {
                InvokePropertyChanged(nameof(IsUrlOk));
                InvokePropertyChanged(nameof(IsRepeatPasswordOk));
            }
        }
    }
}
