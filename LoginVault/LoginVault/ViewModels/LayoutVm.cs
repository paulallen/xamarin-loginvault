﻿using LoginVault.Mvvm;

namespace LoginVault.ViewModels
{
    public class LayoutVm : Observable
    {
        private static readonly object s_padlock = new object();
        private static LayoutVm s_instance;
        public static LayoutVm Instance
        {
            get
            {
                lock (s_padlock)
                {
                    if (s_instance == null)
                    {
                        s_instance = new LayoutVm();
                    }
                }

                return s_instance;
            }
        }

        private bool _showPasswords;
        public bool ShowPasswords
        {
            get { return _showPasswords; }
            set {
                var isChanged = SetField(ref _showPasswords, value);
                if (isChanged) InvokePropertyChanged("HidePasswords");
            }
        }

        public bool HidePasswords
        {
            get { return !_showPasswords; }
        }

        private LayoutVm() { }
    }
}
