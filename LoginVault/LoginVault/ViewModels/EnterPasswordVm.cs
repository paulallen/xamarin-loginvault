﻿using System.Threading.Tasks;
using System.Windows.Input;
using LoginVault.Commands;
using LoginVault.Mvvm;

namespace LoginVault.ViewModels
{
    public class EnterPasswordVm
    {
        public bool IsOk { get; private set; }

        public string VaultName { get; set; }
        public string Password { get; set; }

        public EnterPasswordVm(AppVm appVm)
        {
            _appVm = appVm;
            VaultName = "";
            Password = "";
        }

        public ICommand CmdOk => new RelayCommand(async () => await Close());

        private async Task Close()
        {
            IsOk = true;
            await _appVm.Navigation.PopAsync();
            await _appVm.DoOpenVault(Password);
        }

        private AppVm _appVm;
    }
}
