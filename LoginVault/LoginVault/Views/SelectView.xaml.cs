﻿using LoginVault.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace LoginVault.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SelectView : ContentPage
    {
        public SelectView()
        {
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            if (!(BindingContext is AppVm appVm)) return;

            appVm.DoLoadVaultList();
            base.OnAppearing();
        }
    }
}
