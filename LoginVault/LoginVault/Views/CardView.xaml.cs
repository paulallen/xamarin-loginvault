﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace LoginVault.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CardView : ContentPage
    {
        public CardView()
        {
            InitializeComponent();
        }
    }
}
