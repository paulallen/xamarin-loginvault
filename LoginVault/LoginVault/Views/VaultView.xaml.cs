﻿using LoginVault.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace LoginVault.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class VaultView : ContentPage
    {
        public VaultView()
        {
            InitializeComponent();
        }

        protected override void OnDisappearing()
        {
            if (!(BindingContext is AppVm appVm)) return;

            appVm.DoCloseVault();
            base.OnDisappearing();
        }
    }
}
