﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace LoginVault.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SetPasswordView : ContentPage
    {
        public SetPasswordView()
        {
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            VaultNameEntry.Focus();
        }
    }
}
