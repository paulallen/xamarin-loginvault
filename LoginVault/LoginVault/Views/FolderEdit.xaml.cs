﻿using LoginVault.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace LoginVault.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class FolderEdit : ContentPage
    {
        public FolderEdit()
        {
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            FolderNameEntry.Focus();
        }
    }
}
