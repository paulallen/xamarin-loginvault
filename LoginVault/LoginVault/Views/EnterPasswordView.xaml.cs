﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace LoginVault.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class EnterPasswordView : ContentPage
    {
        public EnterPasswordView()
        {
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            PasswordEntry.Focus();
        }
    }
}
