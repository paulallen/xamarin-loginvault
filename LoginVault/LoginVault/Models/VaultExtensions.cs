﻿using System.Linq;

namespace LoginVault.Models
{
    public static class VaultExtensions
    {
        private const string DELETED_CARDS = "Deleted Cards";

        public static Folder GetBackupsFolder(this Vault vault)
        {
            return vault.Folders.FirstOrDefault(f => f.Name == DELETED_CARDS);
        }

        public static Folder CreateBackupsFolder(this Vault vault)
        {
            Folder folder = new Folder { Name = DELETED_CARDS };
            vault.Folders.Add(folder);
            return folder;
        }

        public static void RemoveBackupsFolder(this Vault vault)
        {
            Folder folder = vault.Folders.FirstOrDefault(f => f.Name == DELETED_CARDS);
            vault.Folders.Remove(folder);
        }
    }
}
