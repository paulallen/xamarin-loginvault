﻿namespace LoginVault.DtoSerialization
{
    public interface IEncrypter
    {
        byte[] Encrypt(string message, string password);
        string Decrypt(byte[] cipher, string password);
    }
}
