﻿namespace LoginVault.DtoSerialization
{
    public interface IPadder
    {
        byte[] Add(byte[] buffer, byte blockSizeBytes);
        byte[] Remove(byte[] paddedBuffer, byte blockSizeBytes);
    }
}
