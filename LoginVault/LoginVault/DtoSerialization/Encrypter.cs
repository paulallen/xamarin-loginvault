﻿using System;
using System.Text;
using Utils;
using static PCLCrypto.WinRTCrypto;

namespace LoginVault.DtoSerialization
{
    public class Encrypter : IEncrypter
    {
        public byte[] Encrypt(string message, string password)
        {
            if (string.IsNullOrEmpty(password))
            {
                throw new Exception("Invalid password");
            }

            IPadder padder = new Padder();

            byte[] ibMsg = CryptographicBuffer.ConvertStringToBinary(message, Encoding.UTF8);
            byte[] ibPadded = padder.Add(ibMsg, BLOCKSIZE_BYTES);
            byte[] ibEncrypted = EncryptBuffer(ibPadded, password);


            // Self-test
            string strTest = Decrypt(ibEncrypted, password);
            Assert.IsTrue(strTest == message, "Self-test error - Decrypt() != message");

            return ibEncrypted;
        }

        public string Decrypt(byte[] cipher, string password)
        {
            if (string.IsNullOrEmpty(password))
            {
                throw new Exception("Invalid password");
            }

            IPadder padder = new Padder();

            byte[] ibDecrypted = DecryptBuffer(cipher, password);
            byte[] ibStripped = padder.Remove(ibDecrypted, BLOCKSIZE_BYTES);

            string str = CryptographicBuffer.ConvertBinaryToString(Encoding.UTF8, ibStripped);
            return str;
        }


        public byte[] EncryptBuffer(byte[] ibPayload, string password)
        {
            Assert.IsTrue(ibPayload.Length % BLOCKSIZE_BYTES == 0, $"EncryptBuffer() - invalid payload.Length: {ibPayload.Length}");
            Assert.IsTrue(!string.IsNullOrEmpty(password), "EncryptBuffer() - invalid password");


            //SymmetricKeyAlgorithmProvider aes = SymmetricKeyAlgorithmProvider.OpenAlgorithm(SymmetricAlgorithmNames.AesCbc);
            var aes = SymmetricKeyAlgorithmProvider.OpenAlgorithm(PCLCrypto.SymmetricAlgorithmName.Aes, PCLCrypto.SymmetricAlgorithmMode.Cbc, PCLCrypto.SymmetricAlgorithmPadding.None);

            Assert.IsTrue(aes.BlockLength == BLOCKSIZE_BYTES, $"EncryptBuffer() - invalid BLOCKSIZE_BYTES: {aes.BlockLength}");
            Assert.IsTrue(aes.BlockLength == INITIAL_VECTOR_LEN, $"EncryptBuffer() - invalid INITIAL_VECTOR_LEN: {aes.BlockLength}");

            // Salt, InitialVector
            byte[] ibPayloadSaltA = CryptographicBuffer.GenerateRandom(PAYLOAD_SALT_A_LEN);
            byte[] ibPasswordSalt = CryptographicBuffer.GenerateRandom(PASSWORD_SALT_LEN);
            byte[] ibInitialVector = CryptographicBuffer.GenerateRandom(INITIAL_VECTOR_LEN);
            byte[] ibPayloadSaltB = CryptographicBuffer.GenerateRandom(PAYLOAD_SALT_B_LEN);

#if DEBUG && LOG_DETAIL
            Logger.D($"EncryptBuffer() - ibPayloadSaltA: '{CryptographicBuffer.EncodeToBase64String(ibPayloadSaltA)}'");
            Logger.D($"EncryptBuffer() - ibPasswordSalt: '{CryptographicBuffer.EncodeToBase64String(ibPasswordSalt)}'");
            Logger.D($"EncryptBuffer() - ibInitialVector: '{CryptographicBuffer.EncodeToBase64String(ibInitialVector)}'");
            Logger.D($"EncryptBuffer() - ibPayloadSaltB: '{CryptographicBuffer.EncodeToBase64String(ibPayloadSaltB)}'");
#endif

            // Password -> key
            byte[] ibPassword = CryptographicBuffer.ConvertStringToBinary(password, Encoding.UTF8);
            byte[] ibSaltedPassword = GenerateSaltedPassword(ibPassword, ibPasswordSalt);
            var key = aes.CreateSymmetricKey(ibSaltedPassword);

            // Encrypt
            byte[] ibEncrypted = CryptographicEngine.Encrypt(key, ibPayload, ibInitialVector);

            // Concat
            byte[] ibAll = AssemblePayload(ibPayloadSaltA, ibPasswordSalt, ibInitialVector, ibEncrypted, ibPayloadSaltB);
            return ibAll;
        }

        public byte[] DecryptBuffer(byte[] ibPayload, string password)
        {
            //SymmetricKeyAlgorithmProvider aes = SymmetricKeyAlgorithmProvider.OpenAlgorithm(SymmetricAlgorithmNames.AesCbc);
            var aes = SymmetricKeyAlgorithmProvider.OpenAlgorithm(PCLCrypto.SymmetricAlgorithmName.Aes, PCLCrypto.SymmetricAlgorithmMode.Cbc, PCLCrypto.SymmetricAlgorithmPadding.None);

            Assert.IsTrue(aes.BlockLength == BLOCKSIZE_BYTES, $"DecryptBuffer() - invalid BlockLength: {aes.BlockLength}");
            Assert.IsTrue(!string.IsNullOrEmpty(password), "DecryptBuffer() - invalid password");


            // Split
            byte[] ibPayloadSaltA, ibPasswordSalt, ibInitialVector, ibPadded, ibPayloadSaltB;
            DisassemblePayload(ibPayload, out ibPayloadSaltA, out ibPasswordSalt, out ibInitialVector, out ibPadded, out ibPayloadSaltB);

#if DEBUG && LOG_DETAIL
            Logger.D($"DecryptBuffer() - ibPayloadSaltA: '{CryptographicBuffer.EncodeToBase64String(ibPayloadSaltA)}'");
            Logger.D($"DecryptBuffer() - ibPasswordSalt: '{CryptographicBuffer.EncodeToBase64String(ibPasswordSalt)}'");
            Logger.D($"DecryptBuffer() - ibInitialVector: '{CryptographicBuffer.EncodeToBase64String(ibInitialVector)}'");
            Logger.D($"DecryptBuffer() - ibPayloadSaltB: '{CryptographicBuffer.EncodeToBase64String(ibPayloadSaltB)}'");
#endif

            // Password -> key
            byte[] ibPassword = CryptographicBuffer.ConvertStringToBinary(password, Encoding.UTF8);
            byte[] ibSaltedPassword = GenerateSaltedPassword(ibPassword, ibPasswordSalt);
            var key = aes.CreateSymmetricKey(ibSaltedPassword);

#if DEBUG && LOG_DETAIL
            Logger.D($"DecryptBuffer() - ibPassword: '{CryptographicBuffer.EncodeToBase64String(ibPassword)}'");
            Logger.D($"DecryptBuffer() - ibSaltedPassword: '{CryptographicBuffer.EncodeToBase64String(ibSaltedPassword)}'");
#endif

            // Decrypt
            byte[] ibDecrypted = CryptographicEngine.Decrypt(key, ibPadded, ibInitialVector);
            return ibDecrypted;
        }


        public byte[] AssemblePayload(byte[] ibPayloadSaltA, byte[] ibPasswordSalt, byte[] ibInitialVector, byte[] ibMsg, byte[] ibPayloadSaltB)
        {
            Assert.IsTrue(ibPayloadSaltA.Length == PAYLOAD_SALT_A_LEN, "AssemblePayload() - invalid saltA length");
            Assert.IsTrue(ibPayloadSaltB.Length == PAYLOAD_SALT_B_LEN, "AssemblePayload() - invalid saltB length");
            Assert.IsTrue(ibPasswordSalt.Length == PASSWORD_SALT_LEN, "AssemblePayload() - invalid salt length");
            Assert.IsTrue(ibInitialVector.Length == BLOCKSIZE_BYTES, "AssemblePayload() - invalid initial vector length");


            //byte[] ibPayload = null;
            //using (DataWriter dw = new DataWriter())
            //{
            //    dw.WriteBuffer(ibPayloadSaltA);
            //    dw.WriteBuffer(ibPasswordSalt);
            //    dw.WriteBuffer(ibInitialVector);
            //    dw.WriteBuffer(ibMsg);
            //    dw.WriteBuffer(ibPayloadSaltB);

            //    ibPayload = dw.DetachBuffer();
            //}

            var ibPayload = new byte[ibPayloadSaltA.Length + ibPasswordSalt.Length + ibInitialVector.Length + ibMsg.Length + ibPayloadSaltB.Length];

            var n = 0;
            for (int i = 0; i < ibPayloadSaltA.Length; ++i)
            {
                ibPayload[n] = ibPayloadSaltA[i];
                ++n;
            }
            for (int i = 0; i < ibPasswordSalt.Length; ++i)
            {
                ibPayload[n] = ibPasswordSalt[i];
                ++n;
            }
            for (int i = 0; i < ibInitialVector.Length; ++i)
            {
                ibPayload[n] = ibInitialVector[i];
                ++n;
            }
            for (int i = 0; i < ibMsg.Length; ++i)
            {
                ibPayload[n] = ibMsg[i];
                ++n;
            }
            for (int i = 0; i < ibPayloadSaltB.Length; ++i)
            {
                ibPayload[n] = ibPayloadSaltB[i];
                ++n;
            }

            // Self-test
            int nCheck = ibPayloadSaltA.Length + ibPasswordSalt.Length + ibInitialVector.Length + ibMsg.Length + ibPayloadSaltB.Length;
            Assert.IsTrue(nCheck == ibPayload.Length, $"AssemblePayload() - assembled payload length: {ibPayload.Length} is not sum: {nCheck}");

            return ibPayload;
        }

        public void DisassemblePayload(byte[] ibPayload, out byte[] ibPayloadSaltA, out byte[] ibPasswordSalt, out byte[] ibInitialVector, out byte[] ibMsg, out byte[] ibPayloadSaltB)
        {
            Assert.IsTrue(ibPayload.Length > PAYLOAD_SALT_A_LEN + PAYLOAD_SALT_B_LEN + PASSWORD_SALT_LEN + INITIAL_VECTOR_LEN + 1, $"DisassemblePayload() - invalid payload length: {ibPayload.Length}");

            int nMsgLen = ibPayload.Length - PAYLOAD_SALT_A_LEN - PAYLOAD_SALT_B_LEN - PASSWORD_SALT_LEN - INITIAL_VECTOR_LEN;
            int uStart = 0;

            ReadBuffer(ibPayload, out ibPayloadSaltA, ref uStart, PAYLOAD_SALT_A_LEN);
            ReadBuffer(ibPayload, out ibPasswordSalt, ref uStart, PASSWORD_SALT_LEN);
            ReadBuffer(ibPayload, out ibInitialVector, ref uStart, INITIAL_VECTOR_LEN);
            ReadBuffer(ibPayload, out ibMsg, ref uStart, nMsgLen);
            ReadBuffer(ibPayload, out ibPayloadSaltB, ref uStart, PAYLOAD_SALT_B_LEN);


            // Self-test
            int uCheck = ibPayloadSaltA.Length + ibPasswordSalt.Length + ibInitialVector.Length + ibMsg.Length + ibPayloadSaltB.Length;
            Assert.IsTrue(uCheck == ibPayload.Length, $"Self-test error - sum of disassembled parts: {uCheck} is not total length: {ibPayload.Length}");
        }


        public void ReadBuffer(byte[] ibFrom, out byte[] ibTo, ref int uStart, int uLength)
        {
            Assert.IsTrue(uStart + uLength <= ibFrom.Length, "ReadBuffer() - invalid uStart, uLength parameter/s");

            //using (DataWriter dw = new DataWriter())
            //{
            //    dw.WriteBuffer(ibFrom, uStart, uLength);
            //    ibTo = dw.DetachBuffer();
            //    uStart += uLength;
            //}

            ibTo = new byte[uLength];
            for (int i = 0; i < uLength; ++i)
            {
                ibTo[i] = ibFrom[i + uStart];
            }
            uStart += uLength;

            Assert.IsTrue(ibTo.Length == uLength, $"ReadBuffer() - unexpected output buffer length: {ibTo.Length}, requested: {uLength}");
        }

        public byte[] GenerateSaltedPassword(byte[] ibPassword, byte[] ibPasswordSalt)
        {
            Assert.IsTrue(ibPassword.Length > 0, "GenerateSaltedPassword() - invalid password parameter");
            Assert.IsTrue(ibPasswordSalt.Length > 0, "GenerateSaltedPassword() - invalid salt parameter");

            //IBuffer ibSalted = null;
            //using (DataWriter dw = new DataWriter())
            //{
            //    dw.WriteBuffer(ibPassword);
            //    dw.WriteBuffer(ibPasswordSalt);

            //    ibSalted = dw.DetachBuffer();
            //}

            int maxLength = 32;
            int newLength = Math.Min(ibPassword.Length + ibPasswordSalt.Length, maxLength);

            var concat = new byte[newLength];

            var n = 0;
            for (int i = 0; i < ibPassword.Length && n < maxLength; ++i)
            {
                concat[n] = ibPassword[i];
                ++n;
            }
            for (int i = 0; i < ibPasswordSalt.Length && n < maxLength; ++i)
            {
                concat[n] = ibPasswordSalt[i];
                ++n;
            }

            return concat;
        }


        private const int BLOCKSIZE_BYTES = 16;
        private const int KEYSIZE_BYTES = 32;
        private const int KEYSIZE_BITS = KEYSIZE_BYTES * 8;

        private const int INITIAL_VECTOR_LEN = BLOCKSIZE_BYTES;
        private const int PASSWORD_SALT_LEN = KEYSIZE_BYTES;
        private const int PAYLOAD_SALT_A_LEN = 13;
        private const int PAYLOAD_SALT_B_LEN = 7;
    }
}
