﻿using System;
using Utils;

namespace LoginVault.DtoSerialization
{
    public class Padder : IPadder
    {
        public byte[] Add(byte[] buffer, byte blockSizeBytes)
        {
            int uMsgLength = buffer.Length;
            int uPaddedSize = PaddedBufferLength(buffer.Length, blockSizeBytes);
            byte bPaddingByte = (byte)(uPaddedSize - uMsgLength);

            Assert.IsTrue(bPaddingByte <= blockSizeBytes, string.Format($"Invalid padding count: {bPaddingByte}"));

#if DEBUG && LOG_DETAIL
            Logger.D($"CreatePaddedBuffer() - padding byte: {bPaddingByte}");
#endif

            //DataWriter writer = new DataWriter();
            //writer.WriteBuffer(buffer);
            //for (int u = uMsgLength; u < uPaddedSize; ++u)
            //{
            //    writer.WriteByte(bPaddingByte);
            //}
            //IBuffer paddedBuffer = writer.DetachBuffer();

            var padded = new byte[uPaddedSize];
            for (int n = 0; n < uMsgLength; ++n)
            {
                padded[n] = buffer[n];
            }
            for (int u = uMsgLength; u < uPaddedSize; ++u)
            {
                padded[u] = bPaddingByte;
            }

            return padded;
        }

        public byte[] Remove(byte[] paddedBuffer, byte blockSizeBytes)
        {
            int uPaddedLength = paddedBuffer.Length;

            //DataReader reader = DataReader.FromBuffer(paddedBuffer);
            //byte[] abPadded = new byte[paddedBuffer.Length];
            //reader.ReadBytes(abPadded);
            //Assert.IsTrue(abPadded.Length == paddedBuffer.Length, "CreateStrippedBuffer() - read wrong length from buffer");

            byte bPaddingByte = paddedBuffer[uPaddedLength - 1];
            int uStrippedLength = (int)(paddedBuffer.Length - bPaddingByte);

#if DEBUG && LOG_DETAIL
            Logger.D($"CreateStrippedBuffer() - padding byte: {bPaddingByte}");
#endif

            Assert.IsTrue(bPaddingByte <= blockSizeBytes, string.Format($"Invalid padding count: {bPaddingByte}"));

            //for (uint uCheck = uStrippedLength; uCheck < uPaddedLength; ++uCheck)
            //{
            //    Assert.IsTrue(abPadded[uCheck] == bPaddingByte, string.Format($"Invalid padding byte :{abPadded[uCheck]}"));
            //}

            //DataWriter writer = new DataWriter();
            //writer.WriteBuffer(paddedBuffer, 0, uStrippedLength);
            //IBuffer ibStripped = writer.DetachBuffer();
            //return ibStripped;

            var stripped = new byte[uStrippedLength];
            for (int n = 0; n < uStrippedLength; ++n)
            {
                stripped[n] = paddedBuffer[n];
            }

            return stripped;
        }

        public int PaddedBufferLength(int messageLength, byte blockSizeBytes)
        {
            int uPaddedLength = messageLength + 1;
            if (uPaddedLength % blockSizeBytes != 0)
            {
                uPaddedLength = ((uPaddedLength + blockSizeBytes) / blockSizeBytes) * blockSizeBytes;
            }
            return uPaddedLength;
        }
    }
}
