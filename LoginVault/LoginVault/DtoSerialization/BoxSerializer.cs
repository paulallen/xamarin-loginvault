﻿using Dtos;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace LoginVault.DtoSerialization
{
    public class BoxSerializer : IBoxSerializer
    {
        XmlSerializer serializer = new XmlSerializer(typeof(Box));

        public string Serialize(Box box)
        {
            using (StringWriter stringwriter = new StringWriter())
            {
                serializer.Serialize(stringwriter, box);
                string str = stringwriter.ToString();
                return str;
            }
        }

        public Box Deserialize(string str)
        {
            using (StringReader stringreader = new StringReader(str))
            {
                var safe = new XmlReaderSettings();
                safe.DtdProcessing = DtdProcessing.Prohibit;
                using (var xmlreader = XmlReader.Create(stringreader, safe))
                {
                    Box box = (Box)serializer.Deserialize(xmlreader);
                    return box;
                }
            }
        }
    }
}
