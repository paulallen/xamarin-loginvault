﻿using Dtos;

namespace LoginVault.DtoSerialization
{
    public interface IBoxSerializer
    {
        string Serialize(Box box);
        Box Deserialize(string str);
    }
}
