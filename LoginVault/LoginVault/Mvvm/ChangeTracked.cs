﻿using System.Runtime.CompilerServices;

namespace LoginVault.Mvvm
{
    public abstract class ChangeTracked : Observable
    {
        public bool IsChanged
        {
            get { return _isChanged; }
            set { SetField(ref _isChanged, value); }
        }

        public ChangeTracked()
            : base()
        {
            IsChanged = false;
        }

        protected bool SetFieldTracked<T>(ref T field, T value, [CallerMemberName] string propertyName = "")
        {
            bool changed = SetField(ref field, value, propertyName);
            if (changed)
            {
                IsChanged = true;
            }
            return changed;
        }

        private bool _isChanged;
    }
}
