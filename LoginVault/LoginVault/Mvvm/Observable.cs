﻿using System;
using System.ComponentModel;
using System.Reflection;
using System.Runtime.CompilerServices;

namespace LoginVault.Mvvm
{
    public abstract class Observable : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        protected void InvokePropertyChanged([CallerMemberName] string propertyName = "")
        {
            if (propertyName == null) { throw new ArgumentNullException(nameof(propertyName)); }

            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        protected bool SetField<T>(ref T field, T value, [CallerMemberName] string propertyName = "")
        {
            if (propertyName == null) { throw new ArgumentNullException(nameof(propertyName)); }

            if ((field == null) || (!field.Equals(value)))
            {
                field = value;
                InvokePropertyChanged(propertyName);

                return true;
            }

            return false;
        }

        protected bool SetProperty<T>(object target, T value, [CallerMemberName] string propertyName = "")
        {
            if (target == null) { throw new ArgumentNullException(nameof(target)); }
            if (propertyName == null) { throw new ArgumentNullException(nameof(propertyName)); }

            PropertyInfo propertyInfo = target.GetType().GetProperty(propertyName);
            if (propertyInfo == null) { throw new ArgumentException($"{nameof(propertyName)} is not a property"); }

            T oldValue = (T)propertyInfo.GetValue(target);
            if ((oldValue == null) || (!oldValue.Equals(value)))
            {
                propertyInfo.SetValue(target, value);
                InvokePropertyChanged(propertyName);

                return true;
            }

            return false;
        }

        // This version doesn't work - I cannot get the interface correct
        //protected virtual void SetPropertyLinq<T>(T value, Expression<Action<T>> outExpr)
        //{
        //    var expr = (MemberExpression)outExpr.Body;
        //    var prop = (PropertyInfo)expr.Member;
        //    prop.SetValue(this, value, null);
        //}
    }
}
