﻿using System;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using LoginVault.ViewModels;
using LoginVault.Views;
using Utils;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace LoginVault
{
    public partial class App : Application
    {
        private readonly AppVm _appVm;

        public App()
        {
            InitializeComponent();

            App.Current.Resources["Layout"] = ViewModels.LayoutVm.Instance;

            MainPage = new NavigationPage(new SelectView());
            _appVm = new AppVm(MainPage.Navigation);

            Device.StartTimer(TimeSpan.FromSeconds(15), () =>
            {
                _appVm.DoAutoSave();
                return true;
            });

            BindingContext = _appVm;
        }

        protected override void OnStart()
        {
            Logger.D($"OnStart");

            AppDomain.CurrentDomain.UnhandledException += CurrentDomainOnUnhandledException;
            TaskScheduler.UnobservedTaskException += TaskSchedulerOnUnobservedTaskException;
        }

        protected override void OnSleep()
        {
            Logger.D($"OnSleep");

            //if (_appVm.AutoSaveService != null)
            //{
            //    var task = _appVm.AutoSaveService.SaveAndStop();
            //}
        }

        protected override void OnResume()
        {
            Logger.D($"OnResume");

            //if (_appVm.AutoSaveService != null)
            //{
            //    var task = _appVm.AutoSaveService.RunAutoSaveLoop();
            //}
        }

        private void TaskSchedulerOnUnobservedTaskException(object sender, UnobservedTaskExceptionEventArgs e)
        {
            OnUnhandled(e.Exception);
        }

        private void CurrentDomainOnUnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            OnUnhandled(e.ExceptionObject as Exception);
        }

        private void OnUnhandled(Exception ex)
        {
            // NB. Displaying unhandled won't work
            Logger.X(Name(), ex);
        }

        private static string Name([CallerMemberName] string name = "")
        {
            return name;
        }
    }
}
