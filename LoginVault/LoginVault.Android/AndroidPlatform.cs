﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

[assembly: Xamarin.Forms.Dependency(typeof(LoginVault.Droid.AndroidPlatform))]
namespace LoginVault.Droid
{
    public class AndroidPlatform : IMyPlatform
    {
        public string GetStorageFolderPath()
        {
            return Android.OS.Environment.ExternalStorageDirectory.AbsolutePath;
        }
    }
}
