﻿using System;

namespace Utils
{
    public static class Assert
    {
        public static void IsTrue(bool fact, string format, params object[] args)
        {
            if (!fact)
            {
                string s = string.Format(System.Globalization.CultureInfo.InvariantCulture, format, args);
                Logger.E(s);
                throw new AssertionException(s);
            }
        }
    }

    public class AssertionException : Exception
    {
        public AssertionException() :
            base()
        {
        }

        public AssertionException(string message) :
            base(message)
        {
        }
    }
}
